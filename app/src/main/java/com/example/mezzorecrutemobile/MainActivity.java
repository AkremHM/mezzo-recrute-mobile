package com.example.mezzorecrutemobile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mezzorecrutemobile.adapters.interfaces.LoginCand;
import com.example.mezzorecrutemobile.adapters.interfaces.LoginEmp;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private Button btnCandidat;
    private Button btnEmploye;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCandidat = findViewById(R.id.btnCandidat);
        btnEmploye = findViewById(R.id.btnEmploye);

        btnCandidat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLoginCandInterface();
            }
        });

        btnEmploye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLoginEmpInterface();
            }
        });
    }

    private void openLoginCandInterface() {
        // Launch the LoginCand interface/activity
        startActivity(new Intent(MainActivity.this, LoginCand.class));
    }

    private void openLoginEmpInterface() {
        // Launch the LoginEmp interface/activity
        startActivity(new Intent(MainActivity.this, LoginEmp.class));

    }

}