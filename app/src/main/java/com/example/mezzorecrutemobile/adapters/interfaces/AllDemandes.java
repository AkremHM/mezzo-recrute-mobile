package com.example.mezzorecrutemobile.adapters.interfaces;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.mezzorecrutemobile.Api.Demande;
import com.example.mezzorecrutemobile.Api.DemandeService;
import com.example.mezzorecrutemobile.Api.Offre;
import com.example.mezzorecrutemobile.Api.OffreService;
import com.example.mezzorecrutemobile.R;
import com.example.mezzorecrutemobile.adapters.DemandeAdapter;
import com.example.mezzorecrutemobile.adapters.OffreAdapter;
import com.example.mezzorecrutemobile.utils.Constans;

import java.util.List;

public class AllDemandes extends AppCompatActivity {
    private DemandeService demandeService;
    private ListView listViewOffres;
    ImageView Text1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_demandes);

        listViewOffres = findViewById(R.id.demandelist);
        Text1 =findViewById(R.id.goback);
        Text1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllDemandes.this, DashboardAdmin.class);
                startActivity(intent);
            }
        });
        // Create Retrofit instance
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constans.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create the service
        demandeService = retrofit.create(DemandeService.class);


        Call<List<Demande>> call = demandeService.getAllDemmandes();
        call.enqueue(new Callback<List<Demande>>() {
            @Override
            public void onResponse(Call<List<Demande>> call, Response<List<Demande>> response) {
                if (response.isSuccessful()) {
                    List<Demande> demandes = response.body();

                    // Create and set the adapter
                    DemandeAdapter demandeAdapter = new DemandeAdapter(AllDemandes.this, demandes);
                    listViewOffres.setAdapter(demandeAdapter);
                } else {
                    // Handle error response
                }
            }

            @Override
            public void onFailure(Call<List<Demande>> call, Throwable t) {
                // Handle API call failure
            }
        });
    }
}
