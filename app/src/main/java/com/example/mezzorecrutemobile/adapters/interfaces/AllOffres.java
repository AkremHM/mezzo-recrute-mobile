package com.example.mezzorecrutemobile.adapters.interfaces;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mezzorecrutemobile.Api.Offre;
import com.example.mezzorecrutemobile.Api.OffreService;
import com.example.mezzorecrutemobile.R;
import com.example.mezzorecrutemobile.adapters.OffreAdapter;
import com.example.mezzorecrutemobile.utils.Constans;

import java.util.List;

public class AllOffres extends AppCompatActivity {
    ImageView Text1;
    private ListView listViewOffres;
    private OffreService offreService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_offres);

        listViewOffres = findViewById(R.id.newslist);
        Text1 =findViewById(R.id.goback);
        Text1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllOffres.this, DashboardAdmin.class);
                startActivity(intent);
            }
        });
        // Create Retrofit instance
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constans.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create the service
        offreService = retrofit.create(OffreService.class);

        // Fetch the list of offers
        Call<List<Offre>> call = offreService.getAllNews();
        call.enqueue(new Callback<List<Offre>>() {
            @Override
            public void onResponse(Call<List<Offre>> call, Response<List<Offre>> response) {
                if (response.isSuccessful()) {
                    List<Offre> offres = response.body();

                    // Create and set the adapter
                    OffreAdapter offreAdapter = new OffreAdapter(AllOffres.this, offres);
                    listViewOffres.setAdapter(offreAdapter);
                } else {
                    // Handle error response
                }
            }

            @Override
            public void onFailure(Call<List<Offre>> call, Throwable t) {
                // Handle API call failure
            }
        });
    }
}
