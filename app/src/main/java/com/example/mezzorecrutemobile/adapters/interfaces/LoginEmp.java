
package com.example.mezzorecrutemobile.adapters.interfaces;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.mezzorecrutemobile.Api.UtilisateurService;
import com.example.mezzorecrutemobile.models.JwtResponse;
import com.example.mezzorecrutemobile.models.LoginRequest;
import com.example.mezzorecrutemobile.utils.Constans;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.example.mezzorecrutemobile.R;

public class LoginEmp extends AppCompatActivity {

    private EditText editTextMatriculeRH, editTextPassword;
    private Button buttonLogin;
    private UtilisateurService utilisateurService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_emp);

        editTextMatriculeRH = findViewById(R.id.email);
        editTextPassword = findViewById(R.id.pwd);
        buttonLogin = findViewById(R.id.connexion);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constans.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        utilisateurService = retrofit.create(UtilisateurService.class);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String matriculeRH = editTextMatriculeRH.getText().toString().trim();
                String password = editTextPassword.getText().toString().trim();

                // Perform validation
                if (TextUtils.isEmpty(matriculeRH)) {
                    editTextMatriculeRH.setError("Matricule RH is required");
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    editTextPassword.setError("Password is required");
                    return;
                }

                if (matriculeRH.length() != 5 || !TextUtils.isDigitsOnly(matriculeRH)) {
                    editTextMatriculeRH.setError("Matricule RH must be 5 digits only");
                    return;
                }

                // Create a LoginRequest object with the entered matricule RH and password
                LoginRequest loginRequest = new LoginRequest(matriculeRH, password);

                // Call the authenticateUser method and handle the response
                Call<JwtResponse> call = utilisateurService.authenticateUser(loginRequest);
                call.enqueue(new Callback<JwtResponse>() {
                    @Override
                    public void onResponse(Call<JwtResponse> call, Response<JwtResponse> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(LoginEmp.this, "Login successful", Toast.LENGTH_SHORT).show();

                            // Handle successful login response
                            JwtResponse jwtResponse = response.body();
                            String username = jwtResponse.getUsername();
                            String role = jwtResponse.getRoles().get(0);
                            String userId = jwtResponse.getId();
                            if (role.equals("ROLE_ADMIN")) {
                                // User has admin role, go to AllOffres activity
                                Intent intent = new Intent(LoginEmp.this, DashboardAdmin.class);                                startActivity(intent);
                            } else if (role.equals("ROLE_RD")) {
                                // User has RD role, go to Profile activity
                                Intent intent = new Intent(LoginEmp.this, DashboardAdmin.class);
                                intent.putExtra("userId", userId); // Pass user ID to the Profile activity
                                startActivity(intent);}

                        } else {
                            // Handle login failure
                            Toast.makeText(LoginEmp.this, "Login failed", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JwtResponse> call, Throwable t) {
                        // Handle login failure
                        Toast.makeText(LoginEmp.this, "Login failed: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
