package com.example.mezzorecrutemobile.adapters.interfaces;


import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.mezzorecrutemobile.Api.UtilisateurService;
import com.example.mezzorecrutemobile.models.JwtResponse;
import com.example.mezzorecrutemobile.models.LoginRequest;
import com.example.mezzorecrutemobile.utils.Constans;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import com.example.mezzorecrutemobile.R;

public class LoginCand extends AppCompatActivity {

    private EditText editTextEmail, editTextPassword;
    private Button buttonLogin;
    private UtilisateurService utilisateurService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_cand);

        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        buttonLogin = findViewById(R.id.buttonLogin);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constans.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        utilisateurService = retrofit.create(UtilisateurService.class);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editTextEmail.getText().toString().trim();
                String password = editTextPassword.getText().toString().trim();

                if (email.isEmpty()) {
                    editTextEmail.setError("Email is required");
                    editTextEmail.requestFocus();
                    return;
                }

                if (!isValidEmail(email)) {
                    editTextEmail.setError("Invalid email");
                    editTextEmail.requestFocus();
                    return;
                }

                if (password.isEmpty()) {
                    editTextPassword.setError("Password is required");
                    editTextPassword.requestFocus();
                    return;
                }

                // Create a LoginRequest object with the entered email and password
                LoginRequest loginRequest = new LoginRequest(email, password);

                // Call the authenticateUser method and handle the response

                Call<JwtResponse> call = utilisateurService.authenticateUser(loginRequest);
                call.enqueue(new Callback<JwtResponse>() {
                    @Override
                    public void onResponse(Call<JwtResponse> call, Response<JwtResponse> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(LoginCand.this, "Login successful", Toast.LENGTH_SHORT).show();
                            JwtResponse jwtResponse = response.body();

                            // Get the user ID from the response or wherever it is stored
                            String userId = jwtResponse.getId();

                            // Pass the user ID to the Profile activity
                            Intent intent = new Intent(LoginCand.this, DashBoardCandidat.class);
                            intent.putExtra("userId", userId);
                            startActivity(intent);
                        } else {
                            // Handle login failure
                            Toast.makeText(LoginCand.this, "Login failed", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JwtResponse> call, Throwable t) {
                        // Handle login failure
                        Toast.makeText(LoginCand.this, "Login failed: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
    }
    private boolean isValidEmail(String email) {
        // Check if the email has "@" and at least one "."
        if (email.contains("@") && email.contains(".") && email.indexOf("@") < email.lastIndexOf(".")) {
            return true;
        }
        return false;
    }
}