package com.example.mezzorecrutemobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.mezzorecrutemobile.Api.Demande;
import com.example.mezzorecrutemobile.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class DemandeAdapter extends ArrayAdapter<Demande> {
    private Context context;
    private List<Demande> demandes;

    public DemandeAdapter(Context context, List<Demande> demandes) {
        super(context, 0, demandes);
        this.context = context;
        this.demandes = demandes;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View itemView = convertView;
        if (itemView == null) {
            itemView = LayoutInflater.from(context).inflate(R.layout.list_item_demandes, parent, false);
        }

        Demande demande = demandes.get(position);

        // Bind the data to the view elements
        TextView textViewTitre = itemView.findViewById(R.id.textViewTitre);
        TextView textViewLieu = itemView.findViewById(R.id.textViewLieu);
        TextView Etat = itemView.findViewById(R.id.Etat);
        // Bind other views as needed

        textViewTitre.setText(demande.getSite());
        textViewLieu.setText(demande.getType_contrat());


        // Bind other data to the views

        return itemView;
    }
}
