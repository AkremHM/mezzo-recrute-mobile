package com.example.mezzorecrutemobile.adapters.interfaces;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.mezzorecrutemobile.R;

public class DashBoardCandidat extends AppCompatActivity {
ImageView cardof;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board_candidat);

        cardof =findViewById(R.id.cardoffre);
        cardof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardCandidat.this, OffreExterne.class);
                startActivity(intent);
            }
        });
    }
}