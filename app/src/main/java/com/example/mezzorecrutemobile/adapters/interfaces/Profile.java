package com.example.mezzorecrutemobile.adapters.interfaces;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mezzorecrutemobile.Api.UtilisateurService;
import com.example.mezzorecrutemobile.Api.Utilisateur;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.example.mezzorecrutemobile.utils.Constans;
import com.example.mezzorecrutemobile.R;
import com.squareup.picasso.Picasso;

public class Profile extends AppCompatActivity {

    private TextView textViewName;
    private TextView textViewEmail;

    private UtilisateurService utilisateurService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constans.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        utilisateurService = retrofit.create(UtilisateurService.class);
        textViewName = findViewById(R.id.textViewName);

        textViewEmail = findViewById(R.id.textViewEmail);

        // Get the user ID from the intent
        Intent intent = getIntent();
        String userId = intent.getStringExtra("userId");

        // Make the API call to retrieve user information
        Call<Utilisateur> call = utilisateurService.findUserById(userId);
        call.enqueue(new Callback<Utilisateur>() {
            @Override
            public void onResponse(Call<Utilisateur> call, Response<Utilisateur> response) {
                if (response.isSuccessful()) {
                    Utilisateur user = response.body();

                    // Populate the TextViews with user information
                    textViewName.setText(user.getUsername());
                    textViewEmail.setText(user.getEmail());


                }
            }

            @Override
            public void onFailure(Call<Utilisateur> call, Throwable t) {
                // Handle API call failure
            }
        });
    }

}
