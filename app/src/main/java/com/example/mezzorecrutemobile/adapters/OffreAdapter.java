package com.example.mezzorecrutemobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.mezzorecrutemobile.R;
import com.example.mezzorecrutemobile.Api.Offre;


import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class OffreAdapter extends ArrayAdapter<Offre> {

    private Context context;
    private List<Offre> offres;

    public OffreAdapter(Context context, List<Offre> offres) {
        super(context, 0, offres);
        this.context = context;
        this.offres = offres;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View itemView = convertView;
        if (itemView == null) {
            itemView = LayoutInflater.from(context).inflate(R.layout.list_item_offre, parent, false);
        }

        Offre offre = offres.get(position);

        // Bind the data to the view elements
        TextView textViewTitre = itemView.findViewById(R.id.titreoffre);
        TextView textViewLieu = itemView.findViewById(R.id.textViewLieu);
        TextView textViewType = itemView.findViewById(R.id.textViewType);
        // Bind other views as needed

        textViewTitre.setText(offre.getTitre());
        textViewLieu.setText(offre.getLieu());
        textViewType.setText(offre.getTypeOffre());
        // Bind other data to the views

        return itemView;
    }
}