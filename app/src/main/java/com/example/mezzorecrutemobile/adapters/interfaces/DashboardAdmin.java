package com.example.mezzorecrutemobile.adapters.interfaces;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.mezzorecrutemobile.R;
public class DashboardAdmin extends AppCompatActivity {
    ImageView cardof;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_admin);

        cardof =findViewById(R.id.cardoffre);
        cardof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardAdmin.this, AllOffres.class);
                startActivity(intent);
            }
        });
    }
}