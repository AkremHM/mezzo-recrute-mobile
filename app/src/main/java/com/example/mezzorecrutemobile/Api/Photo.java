package com.example.mezzorecrutemobile.Api;

import android.graphics.Bitmap;

public class Photo {
    private String id;
    private String title;
    private Bitmap photo;

    public Photo(String id, String title, Bitmap photo) {
        this.id = id;
        this.title = title;
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Bitmap getPhoto() {
        return photo;
    }
}
