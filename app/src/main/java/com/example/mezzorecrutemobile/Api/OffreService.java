package com.example.mezzorecrutemobile.Api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface OffreService {
    @Headers("Content-Type: application/json")

    //get all offre

    @GET("/api/test/offres/getall")
    Call<List<Offre>> getAllNews();

    //get offres externe

    @GET("/api/test/offres/offreexterne")
    Call<List<Offre>> getOffresExterne();

}
