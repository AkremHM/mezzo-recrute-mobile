package com.example.mezzorecrutemobile.Api;

public enum ERole {
    ROLE_EMPLOYEE,
    ROLE_RC,
    ROLE_RECRUTEUR,
    ROLE_RD,
    ROLE_ADMIN,
    ROLE_CANDIDAT
}