package com.example.mezzorecrutemobile.Api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface DemandeService {
    @Headers("Content-Type: application/json")
    @GET("/api/test/demandes/getall")
    Call<List<Demande>> getAllDemmandes();
}