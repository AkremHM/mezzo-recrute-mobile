package com.example.mezzorecrutemobile.Api;
import com.example.mezzorecrutemobile.models.JwtResponse;
import com.example.mezzorecrutemobile.models.LoginRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface UtilisateurService {
    @Headers("Content-Type: application/json")
    @POST("/api/auth/login")
    Call<JwtResponse> authenticateUser(@Body LoginRequest loginRequest);

    @GET("/api/test/utilisateurs/getuser/{id}")
    Call<Utilisateur> findUserById(@Path("id") String id);
    @GET("/api/test/offres/getall")
    Call<List<Offre>> getAllNews();



}
