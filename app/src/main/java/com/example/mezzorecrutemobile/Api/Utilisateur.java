package com.example.mezzorecrutemobile.Api;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.List;

public class Utilisateur implements Serializable {

    private String id;
    private String username;
    private String email;
    private String matriculerh;
    private String password;
    private String adresse;
    private String gender;
    private Long phone;
    private String date_naiss;
    private String cin;
    private List<Photo> photo;


    public Utilisateur() {
    }

    public Utilisateur(String username, String email, String matriculerh, String password,
                       String adresse, String gender, Long phone, String date_naiss, String cin) {
        this.username = username;
        this.email = email;
        this.matriculerh = matriculerh;
        this.password = password;
        this.adresse = adresse;
        this.gender = gender;
        this.phone = phone;
        this.date_naiss = date_naiss;
        this.cin = cin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMatriculerh() {
        return matriculerh;
    }

    public void setMatriculerh(String matriculerh) {
        this.matriculerh = matriculerh;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getDate_naiss() {
        return date_naiss;
    }

    public void setDate_naiss(String date_naiss) {
        this.date_naiss = date_naiss;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public Bitmap getPhoto() {
        if (photo != null && !photo.isEmpty()) {
            Photo firstPhoto = photo.get(0);
            // Assuming the 'photo' field in the Photo class is a Bitmap
            return firstPhoto.getPhoto();
        }
        return null;
    }


    public void setPhoto(List<Photo> photo) {
        this.photo = photo;
    }
}
