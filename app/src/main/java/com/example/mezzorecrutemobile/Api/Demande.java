package com.example.mezzorecrutemobile.Api;

import java.util.Date;

public class Demande {
    private String id;
    private String site;
    private Date date_demande;
    private Date date_integration;
    private String type_contrat;
    private long nbr_collab;
    private String temps_travail;
    private String profile_chercher;
    private String poste_recherche;
    private String description;
    private Date expiration;
    private Validation validation;
    private String remarque;


    private Utilisateur utilisateur;

    public Demande(String id, String site, Date date_demande, Date date_integration, String type_contrat, long nbr_collab, String temps_travail, String profile_chercher, String poste_recherche, String description, Date expiration, Validation validation, String remarque, Utilisateur utilisateur) {
        this.id = id;
        this.site = site;
        this.date_demande = date_demande;
        this.date_integration = date_integration;
        this.type_contrat = type_contrat;
        this.nbr_collab = nbr_collab;
        this.temps_travail = temps_travail;
        this.profile_chercher = profile_chercher;
        this.poste_recherche = poste_recherche;
        this.description = description;
        this.expiration = expiration;
        this.validation = validation;
        this.remarque = remarque;
        this.utilisateur = utilisateur;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Date getDate_demande() {
        return date_demande;
    }

    public void setDate_demande(Date date_demande) {
        this.date_demande = date_demande;
    }

    public Date getDate_integration() {
        return date_integration;
    }

    public void setDate_integration(Date date_integration) {
        this.date_integration = date_integration;
    }

    public String getType_contrat() {
        return type_contrat;
    }

    public void setType_contrat(String type_contrat) {
        this.type_contrat = type_contrat;
    }

    public long getNbr_collab() {
        return nbr_collab;
    }

    public void setNbr_collab(long nbr_collab) {
        this.nbr_collab = nbr_collab;
    }

    public String getTemps_travail() {
        return temps_travail;
    }

    public void setTemps_travail(String temps_travail) {
        this.temps_travail = temps_travail;
    }

    public String getProfile_chercher() {
        return profile_chercher;
    }

    public void setProfile_chercher(String profile_chercher) {
        this.profile_chercher = profile_chercher;
    }

    public String getPoste_recherche() {
        return poste_recherche;
    }

    public void setPoste_recherche(String poste_recherche) {
        this.poste_recherche = poste_recherche;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    public Validation getValidation() {
        return validation;
    }

    public void setValidation(Validation validation) {
        this.validation = validation;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }
}
